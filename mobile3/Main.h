/*
 * Main - main form (interface)
 * Copyright (C) 2014-2015 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MainH
#define MainH 1

#include <FMX.Grid.hpp>
#include <FMX.Header.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Types.hpp>
#include <FMX.ActnList.hpp>
#include <System.Sensors.Components.hpp>
#include <System.Sensors.hpp>
#include <System.Actions.hpp>
#include <System.Classes.hpp>
#include <System.Rtti.hpp>

class TMainForm : public TForm {
__published:
    TToolBar *MainToolBar;
    TLabel *Label1;
    TTabControl *Tabs;
    TTabItem *DataGridTab;
    TStringGrid *DataGrid;
    TStringColumn *StringColumn1;
    TStringColumn *StringColumn2;

    TStyleBook *StyleBook1;
    TActionList *ActionList1;
    TTimer *Refresh;
    TMotionSensor *Accelerometer;
    TOrientationSensor *Compass3D;

    void __fastcall FormShow(TObject *Sender);
    void __fastcall FormHide(TObject *Sender);
    void __fastcall RefreshTimerTimer(TObject *Sender);
    void __fastcall AccelerometerSensorChoosing(TObject *Sender,
            const TSensorArray Sensors, int &ChoseSensorIndex);
    void __fastcall Compass3DSensorChoosing(
            TObject *Sender, const TSensorArray Sensors,
            int &ChoseSensorIndex);

public:
    /*
     * Logs a string message using the FMX logging service if available.
     */
    static void __fastcall Log(UnicodeString Message);

    /*
     * Logs a formatted message using the FMX logging service if available.
     */
    static void __fastcall Log(UnicodeString Format, const TVarRec *Params,
            int Params_High);

    __fastcall TMainForm(TComponent *Owner);
};

extern PACKAGE TMainForm *MainForm;

#endif
