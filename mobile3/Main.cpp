/*
 * Main - main form (implementation)
 * Copyright (C) 2014-2015 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fmx.h>
#pragma hdrstop

#include "Main.h"

#include <cassert>

using namespace std;

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
#pragma resource ("*.NmXhdpiPh.fmx", _PLAT_ANDROID)
#pragma resource ("*.iPhone.fmx", _PLAT_IOS)

TMainForm *MainForm;

/*
 * Returns the platform service of type <code>T</code>.
 */
template <class T>
static DelphiInterface<T> GetPlatformService() {
    return TPlatformServices::Current->GetPlatformService(__uuidof(T));
}

void __fastcall TMainForm::Log(UnicodeString Message) {
    TVarRec params[] = {Message};
    Log(_D("%s"), params, 0);
}

void __fastcall TMainForm::Log(UnicodeString Format, const TVarRec *Params,
        int Params_High) {
    static auto logger = GetPlatformService<IFMXLoggingService>();
    if (logger != NULL) {
        logger->Log(Format, Params, Params_High);
    }
}

//---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent *Owner)
        : TForm(Owner) {
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormShow(TObject *Sender)
{
    Accelerometer->Active = true;
    Compass3D->Active = true;
    if (Accelerometer->Sensor == NULL) {
        ShowMessage("No motion sensor is available.");
        Close();
    } else if (Accelerometer->Sensor->SensorType !=
            TMotionSensorType::Accelerometer3D) {
        ShowMessage("No 3D accelerometer is available.");
        Close();
    } else {
        DataGrid->RowCount = 6;
        DataGrid->Cells[0][0] = _D("AccelerationX");
        DataGrid->Cells[0][1] = _D("AccelerationY");
        DataGrid->Cells[0][2] = _D("AccelerationZ");
        DataGrid->Cells[0][3] = _D("HeadingX");
        DataGrid->Cells[0][4] = _D("HeadingY");
        DataGrid->Cells[0][5] = _D("HeadingZ");
        Refresh->Enabled = true;
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormHide(TObject *Sender)
{
    Refresh->Enabled = false;
    Compass3D->Active = false;
    Accelerometer->Active = false;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::RefreshTimerTimer(TObject *Sender)
{
    static const UnicodeString acceerationlFormat
            = _D("+#0.00;-#0.00; #0.00");
    static const UnicodeString headingFormat
            = _D("+###0.0;-###0.0; ###0.0");

    auto sensor1 = Accelerometer->Sensor;
    assert(sensor1 != NULL);
    DataGrid->Cells[1][0]
            = FormatFloat(acceerationlFormat, sensor1->AccelerationX);
    DataGrid->Cells[1][1]
            = FormatFloat(acceerationlFormat, sensor1->AccelerationY);
    DataGrid->Cells[1][2]
            = FormatFloat(acceerationlFormat, sensor1->AccelerationZ);

    auto sensor2 = Compass3D->Sensor;
    assert(sensor2 != NULL);
    DataGrid->Cells[1][3]
            = FormatFloat(headingFormat, sensor2->HeadingX);
    DataGrid->Cells[1][4]
            = FormatFloat(headingFormat, sensor2->HeadingY);
    DataGrid->Cells[1][5]
            = FormatFloat(headingFormat, sensor2->HeadingZ);

    DataGrid->UpdateColumns();
}

void __fastcall TMainForm::AccelerometerSensorChoosing(TObject *Sender,
        const TSensorArray Sensors, int &ChoseSensorIndex)
{
    for (DynArrInt i = Sensors.Low; i != Sensors.High + 1; ++i) {
        auto sensor = dynamic_cast<TCustomMotionSensor*>(Sensors[i]);
        if (sensor != NULL
                && sensor->SensorType == TMotionSensorType::Accelerometer3D) {
            TVarRec params[] = {i};
            Log("Found a 3D accelerometer (index = %d)", params, 0);
            ChoseSensorIndex = i;
            break;
        }
    }
}

void __fastcall TMainForm::Compass3DSensorChoosing(
        TObject *Sender, const TSensorArray Sensors, int &ChoseSensorIndex)
{
    ChoseSensorIndex = Sensors.High + 1;
    for (auto i = Sensors.Low; i != Sensors.High + 1; ++i) {
        auto &&sensor = dynamic_cast<TCustomOrientationSensor *>(Sensors[i]);
        if (sensor != NULL
                && sensor->SensorType == TOrientationSensorType::Compass3D) {
            TVarRec params[] = {i};
            Log("Found a 3D compass (index = %d)", params, 0);
            ChoseSensorIndex = i;
            break;
        }
    }
}
